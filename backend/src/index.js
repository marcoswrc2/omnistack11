const express = require('express');
const cors = require('cors');
const routes = require('./routes');
const app = express();
app.use(cors());
app.use(express.json());
app.use(routes);

/**
 * Métodos HTTP:
 * GET: Buscar uma informação do Back-end
 * POST:Criar uma informação no back-end
 * PUT: ALTERAR UMA INFORMAÇÃO NO BACK-END
 * DELETE:Deletar uma informação no back-end
 */

/**
 * Tipos de parâmetros:
 *
 * Query params: Parâmetrso nomeados enviados na rota após "?" (Fil tros, paginação)
 * Route Params: Parâmetros utilizados para identificar recursos
 * Request Body: Corpo da requisiçao, utilizado para criar ou alterar recursos!
 */

/**
 * SQL: MySQL, SQLite, PostgreSQL, Oracle, Microsoft SQL Server
 * NoSQ: MongoDB, CouchDB, etc
 */

/**
 * Driver: SELECT * FROM users
 * Query Buider: table("users").select('*').where()
 */

app.listen(3333);
